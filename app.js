const express = require('express')
const mysql = require('mysql')
const app = express()
const port = 3000
const bodyParser = require('body-parser')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const moment = require('moment')
const TOKEN_SECRET = 'fOcJSvA9Z!JiqegF^P0i&mP0JoWhaA6M5qevae$8';

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'testnode'
})

connection.connect()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const authenticateJWT = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
      const token = authHeader.split(' ')[1]
      jwt.verify(token, TOKEN_SECRET, (err, user) => {
          if (err) {
              return res.sendStatus(403);
          }
          next();
      });
  } else {
      res.sendStatus(401);
  }
};

app.post('/register', (req, res) => {
  
  const body = req.body.data
  const {name, email, password} = body

  let response = {};

  if(!name || !email || !password){
    response = {
      statusCode: 400,
      body: JSON.stringify('Missing required fields!')
    }
    return res.send(response)
  }

  connection.query('SELECT * FROM users WHERE email = ?', [email], (err, rows, fields) => {
    if(err) throw err
    const userResult = rows
    if(userResult.length > 0){
      response = {
        statusCode:401,
        body: JSON.stringify('Oops! User already registered')
      }
      res.send(response);
    }else{
      const encryptedPassword = bcrypt.hashSync(password, 10)
      connection.query('INSERT INTO users (name, email, password) VALUES (?,?,?)', [name, email, encryptedPassword], (err, rows, fields) => {
        if(err) throw err
        response = {
          statusCode:200,
          body: JSON.stringify('User inserted successfully!')
        }
        res.send(response);
      })
    }
  })

})

app.post('/login', (req, res) => {

  const body = req.body.data
  const {email, password} = body
  let tokenObject = {}
  let token = ''

  let response = {}

  connection.query('SELECT * FROM users WHERE email = ?', [email], (err, rows, fields) => {
    if (err) throw err
    const userResult = rows
    if(userResult.length > 0){

      const userData = userResult[0]
      const userPassword = userData.password
      const decryptedPassword = bcrypt.compareSync(password, userPassword)

      if(decryptedPassword){
        const userId = userData.id
        token = jwt.sign({
            name: userData.name,
            id: userId
        }, TOKEN_SECRET);
        tokenObject = {
            token: `Bearer ${token}`,
            userId: userId
        }
        response = {
          statusCode: 200,
          body: {"msg":"Logged in successfully!", "token":tokenObject.token}
        }
        res.send(response)
      }else{
        response = {
          statusCode: 401,
          body: JSON.stringify('Oops! Wrong password.'),
        };
        res.send(response)
      }
    }else{
      response = {
        statusCode: 401,
        body: JSON.stringify('Oops! User not found')
      }
      res.send(response)
    }
  })
})

app.get('/tasks', authenticateJWT, (req, res) => {
  let response = {}

  connection.query('SELECT * FROM tasks ORDER BY id DESC', (err, rows, fields) => {
    if(err) throw err
    response = {
      statusCode:200,
      body: rows
    }
    res.send(response)
  })
})

app.post('/tasks', authenticateJWT, (req, res) => {
  
  let response = {}
  const currentDate = moment().format()
  const body = req.body.data
  const {title, description} = body

  if(!title){
    response = {
      statusCode: 400,
      body: JSON.stringify('Missing required fields!')
    }
    return res.send(response)
  }

  connection.query('INSERT INTO tasks (title, status_id, description, created_at, updated_at) VALUES (?,?,?,?,?)', [title, 1, description, currentDate, currentDate], (err, rows, fields) => {
    if(err) throw err
    response = {
      statusCode:200,
      body: JSON.stringify('Task created successfully!')
    }
    res.send(response)
  })
})

app.put('/tasks', authenticateJWT, (req, res) => {
  
  let response = {}
  const currentDate = moment().format()
  const body = req.body.data
  const {id, status} = body

  if(!id || !status){
    response = {
      statusCode: 400,
      body: JSON.stringify('Missing required fields!')
    }
    return res.send(response)
  }

  connection.query('UPDATE tasks SET status_id = ?, updated_at = ? WHERE id = ?', [status, currentDate, id], (err, rows, fields) => {
    if(err) throw err
    response = {
      statusCode:200,
      body: JSON.stringify('Task updated successfully!')
    }
    res.send(response)
  })
})

app.delete('/tasks', authenticateJWT, (req, res) => {
  
  let response = {}
  const currentDate = moment().format()
  const body = req.body.data
  const {id} = body

  if(!id){
    response = {
      statusCode: 400,
      body: JSON.stringify('Missing required fields!')
    }
    return res.send(response)
  }

  connection.query('UPDATE tasks SET status_id = ?, updated_at = ? WHERE id = ?', [4, currentDate, id], (err, rows, fields) => {
    if(err) throw err
    response = {
      statusCode:200,
      body: JSON.stringify('Task deleted successfully!')
    }
    res.send(response)
  })
})

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})
